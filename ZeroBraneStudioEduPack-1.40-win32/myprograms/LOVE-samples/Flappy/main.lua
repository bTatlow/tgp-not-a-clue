
function love.load()
  Init()
end

function love.draw()
  love.graphics.draw(background, backgroundQuad, 0, 0)
  love.graphics.draw(floor, floorQuad, floorPosX, floorPosY)
  love.graphics.draw(floor2, floor2Quad, floor2PosX, floor2PosY)
  love.graphics.draw(flappy, flappyObj:getX() + 18, flappyObj:getY() + 14.5, angle, 1, 1, 18, 14.5)

  for i, v in ipairs(pipeActiveArray) do
    love.graphics.draw(v.imgBot, v.x, v.yBot)          --Draws on bottom screen pipes (array)
    love.graphics.draw(v.imgTop, v.x, v.yTop)          --Draws on top screen pipes (array)
  end
  
  if (gameOver == true) then
    love.graphics.print("GAME OVER", 110, 100, 0, 2, 2)     --Draws when player dies
    love.graphics.print(gameScore, 175, 130, 0, 2, 2)
  else
    love.graphics.print(gameScore, 10, 10, 0, 2, 2)       --Draws current score
  end  
  
  love.graphics.print("High Score:", 150, 10, 0, 2, 2)    --Draws High Score
  love.graphics.print(highScoreS, 300, 10, 0, 2, 2)
  
  if (paused) then
    love.graphics.draw(pauseScreen, 30, 80)
  end
  
  if (mute) then
    love.graphics.print("MUTE", 10, 600, 0, 2, 2)
  end  
end

function love.update(dt)
  Input()   --Handles all user input
  
  if (paused == false and gameOver == false) then       --Check if game is paused
    world:update(dt)            --Update wold physics
    
    Collision()               --Handles all collision events
    
    FlappyDirection()
    
    if (angle <= 1.5) then        --Rotates flappy
      angle = angle + 0.02
    end
    
    floorPosX = floorPosX - gameSpd
    floor2PosX = floor2PosX - gameSpd
    
    if (floorPosX <= -720/2) then
      floorPosX = 720/2
    end
    
    if (floor2PosX <= -720/2) then
      floor2PosX = 720/2
    end
    
    for i, v in ipairs(pipeActiveArray) do    --Loops though the array of on screen pipes
      v.x = v.x - gameSpd                 --Moves pipes (array)
      if (v.x < 65 and v.scored == false) then          --Checks if player is past pipe and has not already been scored
        gameScore = gameScore + 1           --Incriments score by one
        Audio(1)
        v.scored = true               --Prevents pipe being scored multiple time
      end
      
      if (v.x < -75) then               --Check is off screen
        v.x = 375                     --Resets pipes position
        v.scored = false                --Allows pipe to be scored
        pipeActiveArray[i].active = false       --Lets pipe be reselected
        table.remove(pipeActiveArray, i)      --Remove from list of on screen pipes
      end
    end
    
    pipeReleaseCount = pipeReleaseCount + gameSpd
    
    if (pipeReleaseCount >= 250) then     --Check if ready for next pipe
      i = math.random(1, 4)               --Selects a random pipe
      if (pipeArray[i].active == false) then        --Checks if already on screen
        pipeArray[i].active = true                    --Prevents pipe from being reselected
        table.insert(pipeActiveArray, pipeArray[i])       --List of all pipes on screen
        pipeReleaseCount = 0
      end
    end
  end
end

function AABB(x1,y1,w1,h1, x2,y2,w2,h2)       --Check collision
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function Input()
    if (love.keyboard.isDown('space') and jumpKeyDowm == false and paused == false and gameOver == false) then    --Is the jump key pressed
      jumpKeyDowm = true                                              --Prevents event fireing multiple time by acting like a switch
      flappyObj:setLinearVelocity(0, -200)                          --Jump!
      angle = -1
      Audio(2)
    elseif (love.keyboard.isDown('space') == false) then          
        jumpKeyDowm = false                                   --Jump key has been released and allows user to jump again
    end
    
    if (love.keyboard.isDown('escape')) then            
      love.event.quit()                                   --Close game
    end
    
    if (love.keyboard.isDown('p') and pauseKeyDown == false) then
      if (paused == false) then
        paused = true
      else
        paused = false
      end
      pauseKeyDown = true
    elseif (love.keyboard.isDown('p') == false) then
      pauseKeyDown = false
    end
    
    if (love.keyboard.isDown('r') and restartKeyDown == false) then
      restartKeyDown = true
        Restart()
    elseif (love.keyboard.isDown('r') == false) then
      restartKeyDown = false
    end
    
    if (love.keyboard.isDown('m') and muteKeyDown == false) then
      if (mute == false) then
        mute = true
      elseif (mute == true) then
        mute = false
      end
      muteKeyDown = true
    elseif (love.keyboard.isDown('m') == false) then
      muteKeyDown = false
    end
  end
  
  function Collision()
    for i, v in ipairs(pipeActiveArray) do
      if(AABB(v.x, v.yBot, 60, v.hightBot, flappyObj:getX(), flappyObj:getY(), 38, 27)) then      --Check collision with bottom pipe (array)
        gameOver = true                                                   --Collison response
        Audio(3)
        WriteFile(gameScore)
      elseif(AABB(v.x, v.yTop, 60, v.hightTop, flappyObj:getX(), flappyObj:getY(), 38, 27)) then   --Check collision with top pipe (array)
        gameOver = true   
        Audio(3)
        WriteFile(gameScore)
      end
    end
    
    if((flappyObj:getY() + 27) >= floorPosY) then         --Check collision with floor
      gameOver = true
      Audio(3)
      WriteFile(gameScore)
    elseif(flappyObj:getY() <= -27) then              --Check if off of screen
      gameOver = true  
      WriteFile(gameScore)
      Audio(3)
    end
  end
  
function Init()
    world = love.physics.newWorld(1, 1, 0)
    world:setGravity(0, 250)
    
    jumpKeyDowm = false
    restartKeyDown = false
    pauseKeyDown = false
    paused = true
    gameOver = false
    mute = false
    muteKeyDown = false
    
    background = love.graphics.newImage("sprites/bg.png")
    backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
    
    pauseScreen = love.graphics.newImage("sprites/pause.fw.png")
    
    floor = love.graphics.newImage("sprites/ground.png")
    floorSizeX = 720/2
    floorSizeY = 1280/10
    floorPosX = 0
    floorPosY = 1280/2 - 1280/10
    floorQuad = love.graphics.newQuad(1,1,floorSizeX, floorSizeY, floorSizeX, floorSizeY)
    
    floor2 = love.graphics.newImage("sprites/ground.png")
    floor2SizeX = 720/2
    floor2SizeY = 1280/10
    floor2PosX = 720/2
    floor2PosY = 1280/2 - 1280/10
    floor2Quad = love.graphics.newQuad(1,1,floorSizeX, floorSizeY, floorSizeX, floorSizeY)
    
    flappy = love.graphics.newImage("sprites/flappy2.png")     --Initilise flappy
    flappyDown = love.graphics.newImage("sprites/flappy2Down.png")  
    flappyUp = love.graphics.newImage("sprites/flappy2Up.png") 
    flappyPosX = 125
    flappyPosY = 200
    flappyDirection = 0
    flappyPosYPre = flappyPosY
    flappyObj = love.physics.newBody(world, flappyPosX, flappyPosY, 'dynamic')
    
    pipeArray = {}
    pipeActiveArray = {}
    
    for i = 0, 3 do             --Initilise pipes
      local pipe = {}
      pipe.x = 375
      pipe.yBot = 0
      pipe.yTop = 0
      pipe.hightBot = 0
      pipe.hightTop = 0
      pipe.imgBot = love.graphics.newImage("sprites/pipeBot1.png")
      pipe.imgTop = love.graphics.newImage("sprites/pipeTop1.png")
      pipe.active = false
      pipe.scored = false
      table.insert(pipeArray, pipe)
    end
    
    pipeArray[1].imgBot = love.graphics.newImage("sprites/pipeBot4.png")
    pipeArray[1].imgTop = love.graphics.newImage("sprites/pipeTop1.png")
    pipeArray[1].hightBot = 300
    pipeArray[1].hightTop = 74
    pipeArray[1].yBot = floorPosY - pipeArray[1].hightBot
    
    pipeArray[2].imgBot = love.graphics.newImage("sprites/pipeBot3.png")
    pipeArray[2].imgTop = love.graphics.newImage("sprites/pipeTop2.png")
    pipeArray[2].hightBot = 225
    pipeArray[2].hightTop = 150
    pipeArray[2].yBot = floorPosY - pipeArray[2].hightBot
    
    pipeArray[3].imgBot = love.graphics.newImage("sprites/pipeBot2.png")
    pipeArray[3].imgTop = love.graphics.newImage("sprites/pipeTop3.png")
    pipeArray[3].hightBot = 150
    pipeArray[3].hightTop = 225
    pipeArray[3].yBot = floorPosY - pipeArray[3].hightBot
    
    pipeArray[4].imgBot = love.graphics.newImage("sprites/pipeBot1.png")
    pipeArray[4].imgTop = love.graphics.newImage("sprites/pipeTop4.png")
    pipeArray[4].hightBot = 74
    pipeArray[4].hightTop = 300
    pipeArray[4].yBot = floorPosY - pipeArray[4].hightBot  
    
    pipeReleaseCount = 0
    gameSpd = 1.5                 --Speed level moves across screen
    gameScore = 0                --Set game score to 0
    angle = 0                   --Set flappy starting angle
    
    audioScore = love.audio.newSource("audio/score.mp3")
    audioJump = love.audio.newSource("audio/jump.wav")
    audioDeath = love.audio.newSource("audio/splat.wav")
    
    ReadFile()
end

function Restart()
    gameOver = false
    paused = true
    
    flappyObj:setY(flappyPosY)
    flappyObj:setLinearVelocity(0, 0)  
    
    floorPosX = 0
    floor2PosX = 720/2
    
    pipeReleaseCount = 0
    gameSpd = 1.5                 --Speed level moves across screen
    gameScore = 0                --Set game score to 0
    angle = 0
    
    for i, v in ipairs (pipeArray) do
      v.x = 375
      v.active = false
      v.scored = false
    end
    
    table.remove(pipeActiveArray, 1)
    table.remove(pipeActiveArray, 1)
    
    ReadFile()
end

function FlappyDirection()
  direction = flappyObj:getY() - flappyPosYPre    --Calculates change in position
  if (direction < -0.5) then
    flappyDirection = 1               --Flappy going up
  elseif (direction > 0.5) then
    flappyDirection = 3             --Flappy going down
  else
    flappyDirection = 2           --Flappy going nowhere
  end
  flappyPosYPre = flappyObj:getY()
end

function Audio (source)
  if (mute == false) then
    if (source == 1) then
      love.audio.play(audioScore)
    end
    if (source == 2) then
      love.audio.play(audioJump)
    end
    if (source == 3) then
      love.audio.play(audioDeath)
    end
  end
end

function ReadFile()
  highScoreS = love.filesystem.read("highScore.txt")
  highScoreN = tonumber(highScoreS)
end  

function WriteFile(score)
  if (score > highScoreN) then
    love.filesystem.write("highScore.txt", score)
  end
end  