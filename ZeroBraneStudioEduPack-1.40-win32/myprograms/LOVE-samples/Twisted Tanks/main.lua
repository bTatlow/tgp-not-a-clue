function love.load()
  moveCtrl = false
  targetCtrl = false
  levelEnd = false
  pause = false
  options = false
  help = false
  gameOver = false
  start = true
  tankRot = 0
  currentLvl = 1
  
  startMenuArray = {}
  PauseMenuArray = {}
  
  CreatePlayerStats()
  LoadTileMap()
  CreateTileMap(currentLvl)
  InitTurrets()
  CreateCtrlBar()
  CreatePauseMenu()
  CreateStartMenu()
  InitGameAudio()
end

function love.draw()  
  DrawTile()
  DrawMoveTile()
  DrawTargetTile()
  DrawCtrlBar()
  DrawPlayerStats()
  
  tank = love.graphics.newImage("Sprites/Tank.png")
  love.graphics.draw(tank, (((720/2)/7) * 3) + 26, (((720/2)/7) * 5) + 26, tankRot, 0.7, 0.8, 32, 32)
  
  DrawStart()
  DrawPause()
  DrawOptions()
  DrawHelp()
  DrawLvlTransition()
  DrawLevelEndStats()
  DrawGameOverStats()
end

function love.update()
  CheckPlayerDead()
end

function love.mousepressed(x, y)
  if (gameOver == true) then
    currentLvl = 1
    CreatePlayerStats()
    CreateTileMap(currentLvl)
    InitTurrets()
    start = true
    gameOver = false
  end  
  
  if (start == false) then
    for i, v in ipairs (ctrlBarArray) do
      if (x > v.posX and y > v.posY and x < (v.posX + v.width) and y < (v.posY + v.height)) then
        v.click()
      end
    end
  end
  
  if (start == true) then
    for i, v in ipairs (startMenuArray) do
      if (x > v.posX and y > v.posY and x < (v.posX + v.width) and y < (v.posY + v.height)) then
        v.click()
      end
    end
  end
  
    
  if (pause) then
    for i, v in ipairs(PauseMenuArray) do
      if (x > v.posX and y > v.posY and x < (v.posX + v.width) and y < (v.posY + v.height)) then
        v.click()
      end
    end
  end
  
  if (levelEnd == true and pause == false) then
    if (currentLvl < 5) then
      currentLvl = currentLvl + 1
      CreateTileMap(currentLvl)
      InitTurrets()
      
      if (soundOn == true) then
        soundStart:play()
      end
      levelEnd = false
    end
  end
  
  if (moveCtrl) then
    for i, v in ipairs (validMovesArray) do
      if (x > v.posX and y > v.posY and x < (v.posX + v.xy) and y < (v.posY + v.xy)) then
        CalcNumMoves(v.posX, v.posY)
        MovePlayer(v.posX, v.posY)
        if (soundOn == true) then
          soundTankMove:play()
        end
        v.player = true
        tileArray[playerPos].player = false
        moveCtrl = false
        validMovesArray = {}
        player.numMoves = player.numMoves + 1
        CheckEndLvl()
        ShieldPickUp()
        break
      end
    end
  end
  
  if (targetCtrl) then
    for i, v in ipairs (validTargetArray) do
      if (x > v.posX and y > v.posY and x < (v.posX + v.xy) and y < (v.posY + v.xy)) then
        if (v.enemy == true) then
          v.health = v.health - 25
          player.dmgDealt = player.dmgDealt + 25
          CheckTurretDead()
          CheckTileDmg()
          if (soundOn == true) then
            soundTankShot:play()
          end
        end
        targetCtrl = false
      end
    end
  end
end

function DrawTile()
  shield = love.graphics.newImage("Sprites/Shield.PNG")
  
  for i, v in ipairs (tileArray) do
    if (v.shield == false) then
      love.graphics.draw(v.imageBase, v.quad, v.posX, v.posY)
    else
      love.graphics.draw(shield, v.quad, v.posX, v.posY)
    end
  end
end
function DrawMoveTile()
  shieldMove = love.graphics.newImage("Sprites/ShieldValid.PNG")
  
  if (moveCtrl) then  
    for i, v in ipairs (validMovesArray) do
      if (v.shield == false) then
        love.graphics.draw(v.imageValidMove, v.quad, v.posX, v.posY)
      else
        love.graphics.draw(shieldMove, v.quad, v.posX, v.posY)
      end
    end
  end
end
function DrawTargetTile()
    shieldTarget = love.graphics.newImage("Sprites/ShieldShot.PNG")
  
  if (targetCtrl) then  
    for i, v in ipairs (validTargetArray) do
      if (v.shield == false) then
        love.graphics.draw(v.imageValidTarget, v.quad, v.posX, v.posY)
      else
        love.graphics.draw(shieldTarget, v.quad, v.posX, v.posY)
      end
    end
  end
end
function DrawCtrlBar()
  for i, v in ipairs (ctrlBarArray) do
    love.graphics.draw(v.image, v.quad, v.posX, v.posY)
  end
end
function DrawPauseMenu()
  for i, v in ipairs (PauseMenuArray) do
    love.graphics.draw(v.image, v.quad, v.posX, v.posY)
  end
end
function DrawLvlTransition() 
  if (levelEnd == true or gameOver == true) then
    if (currentLvl ~= 5 and gameOver == false) then
      endLvl = love.graphics.newImage("Sprites/LevelTransition.png")
      endLvlQuad = love.graphics.newQuad(0, 0, 360, 640, 360, 640)
      
      love.graphics.draw(endLvl, endLvlQuad)
    else
      endLvl = love.graphics.newImage("Sprites/ScoreGameOver.png")
      endLvlQuad = love.graphics.newQuad(0, 0, 360, 640, 360, 640)
      
      love.graphics.draw(endLvl, endLvlQuad)
    end
  end
end
function DrawPause()
  if (pause == true) then
    pauseGraphic = love.graphics.newImage("Sprites/Paused Menu.png")
    pauseQuad = love.graphics.newQuad(0, 0, 360, 640, 360, 640)
    
    love.graphics.draw(pauseGraphic, pauseQuad)
  end
end
function DrawStart()
  if (start == true) then
    startGraphic = love.graphics.newImage("Sprites/Main Menu.png")
    startQuad = love.graphics.newQuad(0, 0, 360, 640, 360, 640)
    
    love.graphics.draw(startGraphic, startQuad)
  end
end
function DrawOptions()
  if ((pause == true or start == true) and options == true) then
    optionsGraphic = love.graphics.newImage("Sprites/Options Menu.png")
    optionsQuad = love.graphics.newQuad(0, 0, 360, 640, 360, 640)
    
    love.graphics.draw(optionsGraphic, optionsQuad)
  end
end
function DrawHelp()
  if ((pause == true or start == true) and help == true) then
    helpGraphic = love.graphics.newImage("Sprites/HelpMenu.png")
    helpQuad = love.graphics.newQuad(0, 0, 360, 640, 360, 640)
    
    love.graphics.draw(helpGraphic, helpQuad)
  end
end
function DrawPlayerStats()
  statsGraphic = love.graphics.newImage("Sprites/TopHUD.png")
  statsQuad = love.graphics.newQuad(0, 0, 360, 52, 360, 52)
  
  love.graphics.draw(statsGraphic, statsQuad)
  love.graphics.setColor(190, 35, 35)
  font = love.graphics.newFont("Fonts/Computerfont.ttf", 37.5)
  love.graphics.setFont(font)
  love.graphics.print("Health:", 1, 1, 0, 1, 1, -10)
  love.graphics.print(player.health, 1, 1, 0, 1, 1, -120)
  love.graphics.print("Shield:", 1, 1, 0, 1, 1, -190)
  love.graphics.print(player.shield, 1, 1, 0, 1, 1, -300)
  love.graphics.setColor(255, 255, 255)
end
function DrawLevelEndStats()
  if (levelEnd == true) then
    love.graphics.setColor(190, 35, 35)
    font = love.graphics.newFont("Fonts/Computerfont.ttf", 37.5)
    love.graphics.setFont(font)
    love.graphics.print(player.numMoves, 1, 1, 0, 1, 1, -175, -200)
    love.graphics.print(player.numKills, 1, 1, 0, 1, 1, -175, -375)
    love.graphics.setColor(255, 255, 255)
  end
end
function DrawGameOverStats()
  if (gameOver == true) then
    love.graphics.setColor(190, 35, 35)
    font = love.graphics.newFont("Fonts/Computerfont.ttf", 37.5)
    love.graphics.setFont(font)
    love.graphics.print(player.numMoves, 1, 1, 0, 1, 1, -275, -175)
    love.graphics.print(player.numKills, 1, 1, 0, 1, 1, -275, -230)
    
    dmgTaken = 100 - player.health
    score = ((player.numKills * 50) + (currentLvl * 250) + (player.dmgDealt * 25))
    
    if (score >= 20000) then
      grade = "A*"
    elseif (score >= 15000 and score < 20000) then
      grade = "A"
    elseif (score >= 10000 and score < 15000) then
      grade = "B"
    elseif (score >= 5000 and score < 10000) then
      grade = "C"
    elseif (score >= 2500 and score < 5000) then
      grade = "D"
    elseif (score < 2500) then
      grade = "F"
    end
    
    ReadFile()
    
    love.graphics.print(dmgTaken, 1, 1, 0, 1, 1, -275, -290)
    love.graphics.print(score, 1, 1, 0, 1, 1, -260, -350)
    love.graphics.print(grade, 1, 1, 0, 1, 1, -275, -400)
    
    WriteFile(score)
    
    love.graphics.print(highScore, 1, 1, 0, 1, 1, -260, -460)
    love.graphics.setColor(255, 255, 255)
  end
end
function CreateTile(i, x, y) 
  if (i == 0) then
    CreateTilePath(x, y)
  elseif (i == 1) then
    CreateTileWall(x, y)
  elseif (i == 2) then
    CreateTileExit(x, y)
  elseif (i == 3) then
    CreateTileStart(x, y)
  elseif (i == 4) then
    CreateTileEnemyN(x, y)
  elseif (i == 5) then
    CreateTileEnemyE(x, y)
  elseif (i == 6) then
    CreateTileEnemyS(x, y)
  elseif (i == 7) then
    CreateTileEnemyW(x, y)
  end
end
function CreateTilePath(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = false
  tile.player = false
  tile.enemy = false
  tile.shield = false
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/Path.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/PathValid.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/ValidShot.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)
  table.insert(tileArray, tile)
end
function CreateTileWall(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = true
  tile.exit = false
  tile.player = false
  tile.enemy = false
  tile.shield = false
  tile.imageBase = love.graphics.newImage("Sprites/Wall.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)
  table.insert(tileArray, tile)
end
function CreateTileExit(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = true
  tile.player = false
  tile.enemy = false
  tile.shield = false
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/Exit.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/ExitValid.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/ExitShot.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)
  table.insert(tileArray, tile)
end
function CreateTileStart(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = false
  tile.player = true
  tile.enemy = false
  tile.shield = false
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/Start.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/StartValid.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/StartShot.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)
  table.insert(tileArray, tile)
end
function CreateTileMap(mapNum)
  tileArray = {}
  
  for y = 0, 18 do        -- y = up/down
    for x = 0, 17 do       -- x = left/right
      CreateTile(tileMap[mapNum][(y * 18) + (x + 2)], x, y + 1)  -- 7 = width
    end
  end
end
function CreateTileEnemyN(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = false
  tile.player = false
  tile.enemy = true
  tile.shield = false
  tile.direction = 1
  tile.health = 50
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/TurretNorth.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/TurretNorthValid.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/PathValid.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)  
  table.insert(tileArray, tile)
end
function CreateTileEnemyE(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = false
  tile.player = false
  tile.enemy = true
  tile.shield = false
  tile.direction = 2
  tile.health = 50
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/TurretEast.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/TurretEastValid.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/PathValid.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)  
  table.insert(tileArray, tile)
end
function CreateTileEnemyS(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = false
  tile.player = false
  tile.enemy = true
  tile.shield = false
  tile.direction = 3
  tile.health = 50
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/TurretSouth.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/TurretSouthValid.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/PathValid.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)  
  table.insert(tileArray, tile)
end
function CreateTileEnemyW(x, y)
  local tile = {}
  tile.xy = (720/2)/7
  tile.posX = x * tile.xy
  tile.posY = y * tile.xy
  tile.wall = false
  tile.exit = false
  tile.player = false
  tile.enemy = true
  tile.shield = false
  tile.direction = 4
  tile.health = 50
  tile.playerDmg = 0
  tile.imageBase = love.graphics.newImage("Sprites/TurretWest.png")
  tile.imageValidTarget = love.graphics.newImage("Sprites/TurretWestValid.png")
  tile.imageValidMove = love.graphics.newImage("Sprites/PathValid.png")
  tile.quad = love.graphics.newQuad(0, 0, tile.xy, tile.xy, tile.xy, tile.xy)  
  table.insert(tileArray, tile)
end
function LoadTileMap()
  tileMap = {{}, {}, {}, {}, {}}        -- 1 = wall
                                        -- 2 = exit
                                        -- 3 = start
                                        -- 4 = enemy north
                                        -- 5 = enemy east
                                        -- 6 = enemy south
                                        -- 7 = enemy west
                                        -- 0 = path
  
  tileMap[1] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,          
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 3, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 4, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 7, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
              
  tileMap[2] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,          
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 3, 0, 0, 0, 0, 7, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 4, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 7, 0, 0, 0, 0, 0, 2, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
              
  tileMap[3] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,          
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 6, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 2, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
              
  tileMap[4] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,          
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 4, 0, 0, 4, 0, 0, 4, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 7, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 7, 0, 0, 0, 0, 1, 0, 2, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
              
  tileMap[5] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,          
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 5, 0, 0, 0, 1, 0, 0, 0, 1, 2, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1, 0, 0, 0,
                0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
end
function FindPlayer()
  for i, v in ipairs (tileArray) do
    if (v.player == true) then
      playerPos = i
    end
  end
end
function FindExit()
  for i, v in ipairs (tileArray) do
    if (v.exit == true) then
      exitPos = i
    end
  end
end
function ValidMoves()
  validMovesArray = {}
  playerDmgPosX = {}
  playerDmgNegX = {}
  playerDmgPosY = {}
  playerDmgNegY = {}
  
  for i = 1, 3 do
    if (tileArray[playerPos + i].wall == true or tileArray[playerPos + i].enemy == true) then
      break
    end 
    table.insert(validMovesArray, tileArray[playerPos + i])
    if (i > 1) then
      playerDmgPosX[i] = tileArray[playerPos + i].playerDmg + playerDmgPosX[i - 1]
    else
      playerDmgPosX[i] = tileArray[playerPos + i].playerDmg
    end
  end
  
  for i = 1, 3 do
    if (tileArray[playerPos - i].wall == true or tileArray[playerPos - i].enemy == true) then
      break
    end 
    table.insert(validMovesArray, tileArray[playerPos - i])
    if (i > 1) then
      playerDmgNegX[i] = tileArray[playerPos - i].playerDmg + playerDmgNegX[i - 1]
    else
      playerDmgNegX[i] = tileArray[playerPos - i].playerDmg
    end
  end
  
  for i = 1, 5 do
    if (tileArray[playerPos + (i * 18)].wall == true or tileArray[playerPos + (i * 18)].enemy == true) then
      break
    end 
    table.insert(validMovesArray, tileArray[playerPos + (i * 18)])
    if (i > 1) then
      playerDmgPosY[i] = tileArray[playerPos + (i * 18)].playerDmg + playerDmgPosY[i - 1]
    else
      playerDmgPosY[i] = tileArray[playerPos + (i * 18)].playerDmg
    end
  end
  
  for i = 1, 4 do
    if (tileArray[playerPos - (i * 18)].wall == true or tileArray[playerPos - (i * 18)].enemy == true) then
      break
    end 
    table.insert(validMovesArray, tileArray[playerPos - (i * 18)])
    if (i > 1) then
      playerDmgNegY[i] = tileArray[playerPos - (i * 18)].playerDmg + playerDmgNegY[i - 1] 
    else
      playerDmgNegY[i] = tileArray[playerPos - (i * 18)].playerDmg
    end
  end
end
function ValidTarget()
    validTargetArray = {}
  
  for i = 1, 2 do
    if (tileArray[playerPos + i].wall == true) then
      break
    end 
    table.insert(validTargetArray, tileArray[playerPos + i])
  end
  
  for i = 1, 2 do
    if (tileArray[playerPos - i].wall == true) then
      break
    end 
    table.insert(validTargetArray, tileArray[playerPos - i])
  end
  
  for i = 1, 2 do
    if (tileArray[playerPos - (i * 18)].wall == true) then
      break
    end 
    table.insert(validTargetArray, tileArray[playerPos - (i * 18)])
  end
  
  for i = 1, 2 do
    if (tileArray[playerPos + (i * 18)].wall == true) then
      break
    end 
    table.insert(validTargetArray, tileArray[playerPos + (i * 18)])
  end
  
  if (tileArray[playerPos + 19].wall == false) then
    if (tileArray[playerPos + 1].wall == false or tileArray[playerPos + 18].wall == false) then
      table.insert(validTargetArray, tileArray[playerPos + 19])
    end
  end
  
  if (tileArray[playerPos - 19].wall == false) then
    if (tileArray[playerPos - 1].wall == false or tileArray[playerPos - 18].wall == false) then
      table.insert(validTargetArray, tileArray[playerPos - 19])
    end
  end
  
  if (tileArray[playerPos + 17].wall == false) then
    if (tileArray[playerPos - 1].wall == false or tileArray[playerPos + 18].wall == false) then
      table.insert(validTargetArray, tileArray[playerPos + 17])
    end
  end
  
  if (tileArray[playerPos - 17].wall == false) then
    if (tileArray[playerPos + 1].wall == false or tileArray[playerPos - 18].wall == false) then
      table.insert(validTargetArray, tileArray[playerPos - 17])
    end
  end
end
function PlayerMovement()
  FindPlayer()
  ValidMoves()
end
function PlayerTarget()
  FindPlayer()
  ValidTarget()
end
function CreateCtrlBar()
  ctrlBarArray = {}
  
  CreateMoveButton()
  CreateShootButton()
  CreatePauseButton()
end
function CreateMoveButton()
  local button = {}
  button.posX = 0
  button.posY = 565
  button.height = 75
  button.width = 140
  button.image = love.graphics.newImage("Sprites/MoveHUD.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false) then
      if (moveCtrl == false and pause == false) then
        PlayerMovement()
        
        validTargetArray = {}
        moveCtrl = true
        targetCtrl = false
      else
        moveCtrl = false
        validMovesArray = {}
      end
    end
  end
  table.insert(ctrlBarArray, button)
end
function CreateShootButton()
  local button = {}
  button.posX = 140
  button.posY = 565
  button.height = 75
  button.width = 140
  button.image = love.graphics.newImage("Sprites/ShootHUD.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false) then
      if (targetCtrl == false and pause == false) then
        PlayerTarget()
        
        validMovesArray = {}
        moveCtrl = false
        targetCtrl = true
      else
        targetCtrl = false
        validTargetArray = {}
      end
    end
  end
  table.insert(ctrlBarArray, button)
end
function CreatePauseButton()
  local button = {}
  button.posX = 280
  button.posY = 565
  button.height = 75
  button.width = 80
  button.image = love.graphics.newImage("Sprites/OptionsHUD.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false) then
      pause = true
    end
  end
  table.insert(ctrlBarArray, button)
end
function CreatePauseMenu() 
  CreateMuteMusicButton()
  CreateMuteSoundButton()
  CreateResumeButton()
  CreateOptionsButton()
  CreateReturnButton()
  CreateHelpButton()  
end
function CreateResumeButton()
  local button = {}
  button.posX = 90
  button.posY = 150
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false and pause == true and options == false and help == false) then
      pause = false
    end
  end
  table.insert(PauseMenuArray, button)
end
function CreateOptionsButton()
  local button = {}
  button.posX = 90
  button.posY = 235
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false and pause == true) then
      options = true
    end
  end
  table.insert(PauseMenuArray, button)
end
function MovePlayer(newPosX, newPosY)
  for i, v in ipairs (tileArray) do
    v.posX = v.posX - (v.xy * movesNumX)
    v.posY = v.posY - (v.xy * movesNumY)
  end
end
function CalcNumMoves(newPosX, newPosY)
  tileSize = (720/2)/7
  movesNumX = (newPosX - (tileSize * 3)) / tileSize
  movesNumY = (newPosY - (tileSize * 5)) / tileSize
  movesNumXRound = 1
  movesNumYRound = 1
 
  if (movesNumX > 0 and movesNumX < 1.5) then
    movesNumXRound = 1
  elseif (movesNumX > 1.5 and movesNumX < 2.5) then
    movesNumXRound = 2
  elseif (movesNumX > 2.5 and movesNumX < 3.5) then
    movesNumXRound = 3
  elseif (movesNumX > -1.5 and movesNumX < 0) then
    movesNumXRound = 1
  elseif (movesNumX > -2.5 and movesNumX < -1.5) then
    movesNumXRound = 2
  elseif (movesNumX > -3.5 and movesNumX < -2.5) then
    movesNumXRound = 3
  end
  
  if (movesNumY > 0 and movesNumY < 1.5) then
    movesNumYRound = 1
  elseif (movesNumY > 1.5 and movesNumY < 2.5) then
    movesNumYRound = 2
  elseif (movesNumY > 2.5 and movesNumY < 3.5) then
    movesNumYRound = 3
  elseif (movesNumY > 3.5 and movesNumY < 4.5) then
    movesNumYRound = 4
  elseif (movesNumY > 4.5 and movesNumY < 5.5) then
    movesNumYRound = 5
  elseif (movesNumY > -1.5 and movesNumY < 0) then
    movesNumYRound = 1
  elseif (movesNumY > -2.5 and movesNumY < -1.5) then
    movesNumYRound = 2
  elseif (movesNumY > -3.5 and movesNumY < -2.5) then
    movesNumYRound = 3
  elseif (movesNumY > -4.5 and movesNumY < -3.5) then
    movesNumYRound = 4
  elseif (movesNumY > -5.5 and movesNumY < -4.5) then
    movesNumYRound = 5
  end
  
  if (movesNumX > 0) then
    playerDmg = playerDmgPosX[movesNumXRound]
    tankRot = 0
  elseif (movesNumX < 0) then
    playerDmg = playerDmgNegX[movesNumXRound]
    tankRot = 3.14
  elseif (movesNumY > 0) then
    playerDmg = playerDmgPosY[movesNumYRound]
    tankRot = 1.57
  elseif (movesNumY < 0) then
    playerDmg = playerDmgNegY[movesNumYRound]
    tankRot = 4.71
  end
  
  PlayerDmg(playerDmg)
end

function CheckEndLvl()
  FindExit()
  FindPlayer()
  
  if (playerPos == exitPos) then
    if (currentLvl ~= 5) then
    levelEnd = true
    else
    gameOver = true
    end
    if (soundOn == true) then
      soundExit:play()
    end
  end
end
function CreatePlayerStats()
  player = {}
  
  player.health = 100
  player.shield = 100
  player.dmgDealt = 0
  player.numMoves = 0
  player.numKills = 0
end
function InitGameAudio()
  music = love.audio.newSource("Audio/Music.MP3")
  music:setLooping(True)
  music:play()
  musicOn = true
  soundOn = true
  
  soundDeath = love.audio.newSource("Audio/Death.WAV")
  soundStart = love.audio.newSource("Audio/Start.WAV")
  soundExit = love.audio.newSource("Audio/Exit.WAV")
  soundTankExpl = love.audio.newSource("Audio/Tank Explosion.WAV")
  soundTankMove = love.audio.newSource("Audio/Tank Movement.WAV")
  soundTankShot = love.audio.newSource("Audio/TankShot.MP3")
end
function CreateReturnButton()
  local button = {}
  button.posX = 0
  button.posY = 570
  button.height = 70
  button.width = 70
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false and (options == true or help == true)) then
      options = false
      help = false
    end
  end
  table.insert(PauseMenuArray, button)
  table.insert(startMenuArray, button)
end
function CreateHelpButton()
  local button = {}
  button.posX = 90
  button.posY = 320
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false and pause == true and options == false) then
      help = true
    end
  end
  table.insert(PauseMenuArray, button)
end
function CreateMuteMusicButton()
  local button = {}
  button.posX = 90
  button.posY = 235
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false and options == true and help == false) then
      if (musicOn == true) then
        musicOn = false
        music:stop()
      else
        musicOn = true
        music:play()
      end
    end
  end
  table.insert(PauseMenuArray, button)
  table.insert(startMenuArray, button)
end
function CreateMuteSoundButton()
  local button = {}
  button.posX = 90
  button.posY = 150
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (levelEnd == false and options == true and help == false) then
      if (soundOn == true) then
        soundOn = false
      else
        soundOn = true
      end
    end
  end
  table.insert(PauseMenuArray, button)
  table.insert(startMenuArray, button)
end
function PlayerDmg(dmg)
  if (dmg > 0) then
    if (soundOn == true) then
      soundTankShot:play()
    end
    if (player.shield > 0) then
      player.shield = player.shield - dmg
      if (player.shield < 0) then
        healthDmg = player.shield * -1
        player.shield = 0
        player.health = player.health - healthDmg
      end
    else
      player.health = player.health - dmg
    end
  end
end
function InitTurrets()
  for i, v in ipairs(tileArray) do
    if (v.enemy == true) then
      if (v.direction == 1) then
        for j = 1, 3 do
          if (tileArray[i - (j * 18)].wall == true or tileArray[i - (j * 18)].enemy == true) then
            break
          else
            tileArray[i - (j * 18)].playerDmg = tileArray[i - (j * 18)].playerDmg + 25
          end
        end
      end
      
      if (v.direction == 2) then
        for j = 1, 3 do
          if (tileArray[i + j].wall == true or tileArray[i + j].enemy == true) then
            break
          else
            tileArray[i + j].playerDmg = tileArray[i + j].playerDmg + 25
          end
        end
      end
      
      if (v.direction == 3) then
        for j = 1, 3 do
          if (tileArray[i + (j * 18)].wall == true or tileArray[i + (j * 18)].enemy == true) then
            break
          else
            tileArray[i + (j * 18)].playerDmg = tileArray[i + (j * 18)].playerDmg + 25
          end
        end
      end
      
      if (v.direction == 4) then
        for j = 1, 3 do
          if (tileArray[i - j].wall == true or tileArray[i - j].enemy == true) then
            break
          else
            tileArray[i - j].playerDmg = tileArray[i - j].playerDmg + 25
          end
        end
      end
    end
  end
end
function CheckPlayerDead()
  if (player.health <= 0) then
    gameOver = true
    if (soundOn == true) then
      --soundDeath:play()
    end
  end
end
function CheckTurretDead()
    for i, v in ipairs (tileArray) do
      if (v.enemy == true) then
        if (v.health <= 0) then
          if (v.direction == 1) then
            for j = 1, 3 do
              if (tileArray[i - (j * 18)].wall == true or tileArray[i - (j * 18)].enemy == true) then
                break
              else
                tileArray[i - (j * 18)].playerDmg = tileArray[i - (j * 18)].playerDmg - 25
              end
            end
          end
          
          if (v.direction == 2) then
            for j = 1, 3 do
              if (tileArray[i + j].wall == true or tileArray[i + j].enemy == true) then
                break
              else
                tileArray[i + j].playerDmg = tileArray[i + j].playerDmg - 25
              end
            end
          end
          
          if (v.direction == 3) then
            for j = 1, 3 do
              if (tileArray[i + (j * 18)].wall == true or tileArray[i + (j * 18)].enemy == true) then
                break
              else
                tileArray[i + (j * 18)].playerDmg = tileArray[i + (j * 18)].playerDmg - 25
              end
            end
          end
          
          if (v.direction == 4) then
            for j = 1, 3 do
              if (tileArray[i - j].wall == true or tileArray[i - j].enemy == true) then
                break
              else
                tileArray[i - j].playerDmg = tileArray[i - j].playerDmg - 25
              end
            end
          end
          v.enemy = false
          v.shield = true
          if (soundOn == true) then
            soundTankExpl:play()
          end
          player.numKills = player.numKills + 1
        end
      end
    end
  end
function CheckTileDmg()
  FindPlayer()
  playerDmg = tileArray[playerPos].playerDmg
  PlayerDmg(playerDmg)
end
function CreateStartMenu()
  CreateStartOptionsButton()
  CreateStartHelpButton()
  CreateStartButton()
end
function CreateStartOptionsButton()
  local button = {}
  button.posX = 90
  button.posY = 235
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (start == true) then
      options = true
    end
  end
  table.insert(startMenuArray, button)
end
function CreateStartHelpButton()
  local button = {}
  button.posX = 90
  button.posY = 150
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (start == true and options == false) then
      help = true
    end
  end
  table.insert(startMenuArray, button)
end
function CreateStartButton()
  local button = {}
  button.posX = 90
  button.posY = 65
  button.height = 70
  button.width = 180
  button.image = love.graphics.newImage("Sprites/TestTile.png")
  button.quad = love.graphics.newQuad(0, 0, button.width, button.height, button.width, button.height)
  button.click = function()
    if (start == true) then
      start = false
    end
  end
  table.insert(startMenuArray, button)
end
function ReadFile()
  hs = love.filesystem.read("score.txt")
  highScore = tonumber(hs)
end
function WriteFile(score)
  if (score > highScore) then
    highScore = score
    love.filesystem.write("score.txt", highScore)
  end
end

function ShieldPickUp()
  for i, v in ipairs(tileArray) do
    if (v.shield == true and playerPos == i) then
      v.shield = false
      v.imageBase = love.graphics.newImage("Sprites/Path.png")
      v.imageValidTarget = love.graphics.newImage("Sprites/ValidShot.png")
      player.shield = player.shield + 20
    end
  end
end
